#include <iostream>

void sortString(std::string &str) {
    sort(str.begin(), str.end());
}

int main() {
    std::string str1 = "abc";
    std::string str2 = "cab";
    sortString(str1);
    sortString(str2);

    for (unsigned long i = 0; i < str1.size(); i++) {
        if (str1[i] != str2[i]) {
            std::cout << "Strings are not permutations of each other." << std::endl;
            return 0;
        }
    }
    std::cout << "Strings are permutations of each other!" << std::endl;
    return 0;
}