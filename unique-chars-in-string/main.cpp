#include <iostream>
using namespace std;

int main() {
	string str1 = "abcdefg";
	for (unsigned long i = 0; i < str1.size(); i++) {
		for (unsigned long j = str1.size() - 1; j > i; j--) {
			if (str1[i] == str1[j]) {
				cout << str1 << " has not all unique chars!" << endl;
				return 0;
			}
		}
	}
	cout << "String " << str1 << " is unique!" << endl;
	return 0;
}
